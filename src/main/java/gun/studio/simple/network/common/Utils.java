package gun.studio.simple.network.common;

import gun.studio.simple.network.SimpleServer;

public class Utils {
    private static final String TAG = Utils.class.getSimpleName();

    public static void log(String tag, String msg) {
        if (SimpleServer.getLogListener() != null) {
            SimpleServer.getLogListener().log(tag, msg);
        }
    }

    public static void error(String tag, Throwable ex) {
        if (SimpleServer.getLogListener() != null) {
            SimpleServer.getLogListener().error(tag, ex);
        }
    }
}
