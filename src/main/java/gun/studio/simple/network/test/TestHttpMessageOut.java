package gun.studio.simple.network.test;

import gun.studio.simple.network.http.HttpMessage;
import gun.studio.simple.network.http.HttpMessageOut;

@HttpMessage(name = "TEST_HTTP_OUT")
public class TestHttpMessageOut extends HttpMessageOut {
    public int value = 1;
}
