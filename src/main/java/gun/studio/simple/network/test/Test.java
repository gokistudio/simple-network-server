package gun.studio.simple.network.test;

import gun.studio.simple.network.*;
import gun.studio.simple.network.socketio.SocketIOListener;

public class Test {
    public static void main(String[] args) {
        SimpleServer.setSocketIOListener(new SocketIOListener() {
            @Override
            public void onSessionConnect(Session session) {
                System.out.println("onSessionConnect: " + session.getSessionId());
            }

            @Override
            public void onSessionDisconnect(Session session) {
                System.out.println("onSessionDisconnect: " + session.getSessionId());
            }
        });
        SimpleServer.setLogListener(new LogListener() {
            @Override
            public void log(String tag, String msg) {
                System.out.println("log: " + msg);
            }

            @Override
            public void error(String tag, Exception ex) {
                ex.printStackTrace();
            }

            @Override
            public void error(String tag, Throwable ex) {
                ex.printStackTrace();
            }
        });
        SimpleServer.init();
        SimpleServer.startSocketIO(12345);//start socketio
        SimpleServer.startHttp(23456);//start http
    }
}
