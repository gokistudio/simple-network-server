package gun.studio.simple.network.test;

import gun.studio.simple.network.common.Utils;
import gun.studio.simple.network.socketio.Message;
import gun.studio.simple.network.socketio.MessageIn;

@Message(name = "CS_CHAT")
public class CSChat extends MessageIn {
    private static final String TAG = CSChat.class.getSimpleName();

    public String text;

    @Override
    public void execute() throws Exception {
        Utils.log(TAG, "Tesssssssssssssst1: " + text);
        SCChat SCChat = new SCChat();
        SCChat.text = "Hello Client";
        sendMessage(SCChat);
    }
}
