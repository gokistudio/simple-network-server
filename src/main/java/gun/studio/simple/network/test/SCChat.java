package gun.studio.simple.network.test;

import gun.studio.simple.network.socketio.Message;
import gun.studio.simple.network.socketio.MessageIn;
import gun.studio.simple.network.socketio.MessageOut;

@Message(name = "SC_CHAT")
public class SCChat extends MessageOut {
    public String text;
}
