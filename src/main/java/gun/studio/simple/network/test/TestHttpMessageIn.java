package gun.studio.simple.network.test;

import gun.studio.simple.network.http.HttpMessage;
import gun.studio.simple.network.http.HttpMessageIn;

@HttpMessage(name = "TEST_HTTP_IN")
public class TestHttpMessageIn extends HttpMessageIn {
    public int value;

    @Override
    public void execute() throws Exception {
        TestHttpMessageOut testHttpMessageOut = new TestHttpMessageOut();
        testHttpMessageOut.value = value + 1;
        sendMessage(testHttpMessageOut);
    }
}
