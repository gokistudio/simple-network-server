package gun.studio.simple.network;

import gun.studio.simple.network.http.HttpServer;
import gun.studio.simple.network.http.HttpMessageManager;
import gun.studio.simple.network.socketio.MessageManager;
import gun.studio.simple.network.socketio.SocketIOListener;
import gun.studio.simple.network.socketio.SocketIOServer;

public class SimpleServer {
    public static SocketIOListener socketIOListener;
    public static LogListener logListener;

    public static void init() {
        MessageManager.init();
        HttpMessageManager.init();
    }

    public static SocketIOListener getSocketIOListener() {
        return socketIOListener;
    }

    public static void setSocketIOListener(SocketIOListener socketIOListener) {
        SimpleServer.socketIOListener = socketIOListener;
    }

    public static LogListener getLogListener() {
        return logListener;
    }

    public static void setLogListener(LogListener logListener) {
        SimpleServer.logListener = logListener;
    }

    public static void startSocketIO(int port) {
        SocketIOServer.start(port);
    }

    public static void startHttp(int port) {
        HttpServer.start(port);
    }
}
