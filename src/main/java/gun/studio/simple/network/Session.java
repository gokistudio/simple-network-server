package gun.studio.simple.network;

import com.corundumstudio.socketio.SocketIOClient;
import com.fasterxml.jackson.databind.ObjectMapper;
import gun.studio.simple.network.common.Utils;
import gun.studio.simple.network.socketio.MessageIn;
import gun.studio.simple.network.socketio.MessageManager;
import gun.studio.simple.network.socketio.MessageOut;
import io.netty.channel.Channel;
import org.apache.commons.lang3.RandomStringUtils;
import org.json.JSONObject;

public class Session {
    private static final String TAG = Session.class.getSimpleName();

    private String sessionId;
    private SocketIOClient client;
    private Channel channel;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public void genSessionIdFromClient() {
        sessionId = getChannelId().substring(0, 16);//set sessionId giống clientId check cho dễ
        if (SessionManager.getSessionBySessionId(sessionId) != null) {
            while (true) {
                sessionId = RandomStringUtils.randomAlphanumeric(8);
                if (SessionManager.getSessionBySessionId(sessionId) == null) {
                    break;
                }
            }
        }
    }

    public void setClient(SocketIOClient client) {
        this.client = client;
    }

    public String getChannelId() {
        if (client != null) {
            return client.getSessionId().toString();
        } else if (channel != null) {
            return channel.id().toString();
        }
        return null;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public String getIp() {
        String ip = client.getRemoteAddress().toString();
        ip = ip.replaceAll("/", "");
        String[] strs = ip.split("\\:");
        if (strs.length > 0) ip = strs[0];
        return ip;
    }

    public void sendMessage(MessageOut messageOut) {
        messageOut.setSession(this);
        if (messageOut.log()) {
            Utils.log(TAG, "Send Msg: " + messageOut.toString());
        }
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("name", messageOut.messageName());
            jsonObject.put("body", objectMapper.writeValueAsString(messageOut));
            client.sendEvent("msg", jsonObject.toString());
        } catch (Exception e) {
            Utils.error(TAG, e);
        }
    }

    public void receiverMessage(MessageIn messageIn) {
        messageIn.setSession(this);
        if (messageIn.log()) {
            Utils.log(TAG, "Receiver Msg: " + messageIn.toString());
        }
        try {
            messageIn.execute();
        } catch (Exception e) {
            Utils.error(TAG, e);
        }
    }
}
