package gun.studio.simple.network;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class SessionManager {
    private static final String TAG = SessionManager.class.getSimpleName();

    private static Map<String, Session> sessionByChannelIdMap = new ConcurrentHashMap<>();
    private static Map<String, Session> sessionBySessionIdMap = new ConcurrentHashMap<>();

    public static void addSession(Session session) {
        sessionByChannelIdMap.put(session.getChannelId(), session);
        sessionBySessionIdMap.put(session.getSessionId(), session);
    }

    public static Session getSessionByClientId(String clientId) {
        return sessionByChannelIdMap.get(clientId);
    }

    public static Session getSessionByChannelId(String channelId) {
        return sessionByChannelIdMap.get(channelId);
    }

    public static Session getSessionBySessionId(String sessionId) {
        return sessionBySessionIdMap.get(sessionId);
    }

    public static void removeSession(Session session) {
        if (session.getSessionId() != null) sessionBySessionIdMap.remove(session.getSessionId());
        if (session.getChannelId() != null) sessionByChannelIdMap.remove(session.getChannelId());
    }
}
