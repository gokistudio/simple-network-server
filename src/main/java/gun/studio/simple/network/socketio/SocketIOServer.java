package gun.studio.simple.network.socketio;

import com.corundumstudio.socketio.AckRequest;
import com.corundumstudio.socketio.Configuration;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.listener.ConnectListener;
import com.corundumstudio.socketio.listener.DataListener;
import com.corundumstudio.socketio.listener.DisconnectListener;
import com.fasterxml.jackson.databind.ObjectMapper;
import gun.studio.simple.network.Session;
import gun.studio.simple.network.SessionManager;
import gun.studio.simple.network.SimpleServer;
import gun.studio.simple.network.common.Utils;
import org.json.JSONObject;

import java.io.FileInputStream;
import java.io.InputStream;

public class SocketIOServer {
    private static final String TAG = SocketIOServer.class.getSimpleName();

    public static void start(int port) {
        start(port, null, null);
    }

    public static void start(int port, String jksFile, String jksPass) {
        Utils.log(TAG, "WebSocketListener Start");
        InputStream inputStream = null;
        try {
            Configuration cofiguration = new Configuration();
            cofiguration.setPort(port);
            cofiguration.setPingTimeout(60000);
            cofiguration.setPingInterval(10000);
            cofiguration.setWorkerThreads(12);
            cofiguration.setBossThreads(2);
            cofiguration.getSocketConfig().setTcpKeepAlive(true);
            cofiguration.getSocketConfig().setReuseAddress(true);
            //cofiguration.setHostname("::");
            cofiguration.setHostname("0.0.0.0");
            if (jksFile != null) {
                cofiguration.setKeyStorePassword(jksPass);
                FileInputStream fileInputStream = new FileInputStream(jksFile);
                cofiguration.setKeyStore(fileInputStream);
            }

            com.corundumstudio.socketio.SocketIOServer socketIOServer = new com.corundumstudio.socketio.SocketIOServer(cofiguration);

            socketIOServer.addConnectListener(new ConnectListener() {
                public void onConnect(SocketIOClient client) {
                    try {
                        Session session = new Session();
                        session.setClient(client);
                        session.genSessionIdFromClient();
                        SessionManager.addSession(session);
                        if (SimpleServer.getSocketIOListener() != null) {
                            SimpleServer.getSocketIOListener().onSessionConnect(session);
                        }
                    } catch (Exception e) {
                        Utils.error(TAG, e);
                    }
                }
            });

            socketIOServer.addDisconnectListener(new DisconnectListener() {
                public void onDisconnect(SocketIOClient client) {
                    try {
                        Session session = SessionManager.getSessionByClientId(client.getSessionId().toString());
                        if (session != null) {
                            SessionManager.removeSession(session);
                            if (SimpleServer.getSocketIOListener() != null) {
                                SimpleServer.getSocketIOListener().onSessionDisconnect(session);
                            }
                        }
                    } catch (Exception e) {
                        Utils.error(TAG, e);
                    }
                }
            });

            ObjectMapper objectMapper = new ObjectMapper();
            socketIOServer.addEventListener("msg", Object.class, new DataListener<Object>() {
                public void onData(SocketIOClient client, Object data, AckRequest arg2) throws Exception {
                    try {
                        JSONObject jsonObject = new JSONObject(String.valueOf(data));
                        String messageName = jsonObject.getString("name");
                        Session session = SessionManager.getSessionByClientId(client.getSessionId().toString());
                        if (session != null) {
                            String messageBody = jsonObject.get("body").toString();
                            Class clss = MessageManager.getClassMessageIn(messageName);
                            MessageIn messageIn = (MessageIn) objectMapper.readValue(messageBody, clss);
                            session.receiverMessage(messageIn);
                        }
                    } catch (Exception e) {
                        Utils.log(TAG, "receiverMessage: " + data);
                        Utils.error(TAG, e);
                    }
                }
            });

            socketIOServer.start();
            Utils.log(TAG, "SocketIO listening on " + port);
        } catch (Exception e) {
            Utils.log(TAG, "SocketIO listening error on " + port);
            Utils.error(TAG, e);
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
