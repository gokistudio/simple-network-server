package gun.studio.simple.network.socketio;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface Message {
    public String name() default "";

    public boolean log() default true;
}
