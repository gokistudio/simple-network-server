package gun.studio.simple.network.socketio;

public abstract class MessageIn extends MessageBase {
    public abstract void execute() throws Exception;
}
