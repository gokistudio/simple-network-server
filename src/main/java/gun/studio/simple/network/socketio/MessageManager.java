package gun.studio.simple.network.socketio;

import gun.studio.simple.network.common.Utils;
import io.netty.util.internal.ConcurrentSet;
import org.reflections.Reflections;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class MessageManager {
    private static final String TAG = MessageManager.class.getSimpleName();

    private static Map<String, Class> messageInMap = new ConcurrentHashMap<>();
    private static Map<String, Class> messageOutMap = new ConcurrentHashMap<>();

    public static void init() {
        Utils.log(TAG, "###################### LOAD MESSAGE CLASS ######################");
        messageInMap.clear();
        messageOutMap.clear();
        Reflections reflections = new Reflections();
        Set<Class<?>> classes = reflections.getTypesAnnotatedWith(Message.class);
        for (Class<?> clss : classes) {
            try {
                Message message = clss.getAnnotation(Message.class);
                if (message != null) {
                    if (clss.newInstance() instanceof MessageIn) {
                        if (messageInMap.containsKey(message.name())) {
                            Exception exception = new RuntimeException("Dupplicate Message: " + clss.getName());
                            Utils.error(TAG, exception);
                            throw exception;
                        } else {
                            messageInMap.put(message.name(), clss);
                        }
                    } else if (clss.newInstance() instanceof MessageOut) {
                        if (messageOutMap.containsKey(message.name())) {
                            Exception exception = new RuntimeException("Dupplicate Message: " + clss.getName());
                            Utils.error(TAG, exception);
                            throw exception;
                        } else {
                            messageOutMap.put(message.name(), clss);
                        }
                    }
                    Utils.log(TAG, "Reload Message: " + clss.getName());
                } else {
                    Exception exception = new RuntimeException("Cannot found Message: " + clss.getName());
                    Utils.error(TAG, exception);
                    throw exception;
                }
            } catch (Exception e) {
                Utils.error(TAG, e);
            }
        }
    }

    public static Class getClassMessageIn(String messageName) throws Exception {
        if (messageInMap.containsKey(messageName)) {
            Class clss = messageInMap.get(messageName);
            return clss;
        }
        return null;
    }

    public static MessageIn getMessageIn(String messageName) throws Exception {
        if (messageInMap.containsKey(messageName)) {
            Class clss = messageInMap.get(messageName);
            return (MessageIn) clss.newInstance();
        }
        return null;
    }
}
