package gun.studio.simple.network.socketio;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import gun.studio.simple.network.Session;
import gun.studio.simple.network.common.Utils;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MessageBase {
    private static final String TAG = MessageBase.class.getSimpleName();

    @JsonIgnore
    private Session session;

    public String messageName() {
        Message message = getClass().getAnnotation(Message.class);
        if (message != null) {
            return message.name();
        }
        return "";
    }

    public boolean log() {
        Message message = getClass().getAnnotation(Message.class);
        if (message != null) {
            return message.log();
        }
        return true;
    }

    @JsonIgnore
    public String getSessionId() {
        if (getSession() == null) return null;
        return getSession().getSessionId();
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public void sendMessage(MessageOut messageOut) {
        session.sendMessage(messageOut);
    }

    public void receiverMessage(MessageIn messageIn) {
        session.receiverMessage(messageIn);
    }

    @Override
    public String toString() {
        try {
            StringBuilder builder = new StringBuilder();
            ObjectMapper objectMapper = new ObjectMapper();
            builder.append(messageName() + " | " + objectMapper.writeValueAsString(this));
            if (session != null) {
                if (session.getSessionId() != null) {
                    builder.append(" | sessionId=" + session.getSessionId());
                } else {
                    builder.append(" | sessionId=" + session.getChannelId());
                }
            }
            return builder.toString();
        } catch (Exception e) {
            Utils.error(TAG, e);
        }
        return super.toString();
    }
}
