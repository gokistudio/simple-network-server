package gun.studio.simple.network.socketio;

import gun.studio.simple.network.Session;

public interface SocketIOListener {
    public void onSessionConnect(Session session);

    public void onSessionDisconnect(Session session);
}
