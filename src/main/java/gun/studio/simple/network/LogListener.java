package gun.studio.simple.network;

public interface LogListener {
    public void log(String tag, String msg);

    public void error(String tag, Exception ex);

    public void error(String tag, Throwable ex);
}
