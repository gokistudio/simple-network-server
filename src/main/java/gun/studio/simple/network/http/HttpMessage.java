package gun.studio.simple.network.http;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface HttpMessage {
    public String name() default "";

    public boolean log() default true;
}
