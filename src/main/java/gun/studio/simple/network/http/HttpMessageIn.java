package gun.studio.simple.network.http;

public abstract class HttpMessageIn extends HttpMessageBase {
    public abstract void execute() throws Exception;
}
