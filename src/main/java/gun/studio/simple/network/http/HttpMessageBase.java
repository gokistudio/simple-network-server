package gun.studio.simple.network.http;

import com.fasterxml.jackson.databind.ObjectMapper;
import gun.studio.simple.network.common.Utils;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.*;
import io.netty.util.CharsetUtil;
import org.json.JSONObject;

public abstract class HttpMessageBase {
    private static final String TAG = HttpMessageBase.class.getSimpleName();

    private ChannelHandlerContext ctx;

    public ChannelHandlerContext getCtx() {
        return ctx;
    }

    public void setCtx(ChannelHandlerContext ctx) {
        this.ctx = ctx;
    }

    public String messageName() {
        HttpMessage message = getClass().getAnnotation(HttpMessage.class);
        if (message != null) {
            return message.name();
        }
        return "";
    }

    public boolean log() {
        HttpMessage message = getClass().getAnnotation(HttpMessage.class);
        if (message != null) {
            return message.log();
        }
        return true;
    }

    public void sendMessage(HttpMessageOut messageOut) {
        if (messageOut.log()) {
            Utils.log(TAG, "Send HttpMsg: " + messageOut.toString());
        }
        try {
            JSONObject dataJson = new JSONObject();
            ObjectMapper objectMapper = new ObjectMapper();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("name", messageOut.messageName());
            jsonObject.put("body", objectMapper.writeValueAsString(messageOut));
            dataJson.put("data", jsonObject);

            ByteBuf content = Unpooled.copiedBuffer(dataJson.toString(), CharsetUtil.UTF_8);
            FullHttpResponse httpResponse = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK, content);
            httpResponse.headers().set(HttpHeaderNames.CONTENT_TYPE, "application/json;charset=UTF-8");
            httpResponse.headers().set(HttpHeaderNames.CONTENT_LENGTH, content.readableBytes());
            httpResponse.headers().set("Access-Control-Allow-Origin", "*");
            httpResponse.headers().set("Access-Control-Allow-Methods", "GET,POST");
            httpResponse.headers().set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            ctx.write(httpResponse);
            Utils.log(TAG, "Cannot find HttpMessageIn");
        } catch (Exception e) {
            Utils.error(TAG, e);
        }
    }
}
