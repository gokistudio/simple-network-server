package gun.studio.simple.network.http;

import gun.studio.simple.network.common.Utils;
import io.netty.util.internal.ConcurrentSet;
import org.reflections.Reflections;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class HttpMessageManager {
    private static final String TAG = HttpMessageManager.class.getSimpleName();

    private static Map<String, Class> messageInMap = new ConcurrentHashMap<>();
    private static Map<String, Class> messageOutMap = new ConcurrentHashMap<>();

    public static void init() {
        Utils.log(TAG, "###################### LOAD HTTP_MESSAGE CLASS ######################");
        messageInMap.clear();
        messageOutMap.clear();
        Reflections reflections = new Reflections();
        Set<Class<?>> classes = reflections.getTypesAnnotatedWith(HttpMessage.class);
        for (Class<?> clss : classes) {
            try {
                HttpMessage message = clss.getAnnotation(HttpMessage.class);
                if (message != null) {
                    if (clss.newInstance() instanceof HttpMessageIn) {
                        if (messageInMap.containsKey(message.name())) {
                            Exception exception = new RuntimeException("Dupplicate HttpMessage: " + clss.getName());
                            Utils.error(TAG, exception);
                            throw exception;
                        } else {
                            messageInMap.put(message.name(), clss);
                        }
                    } else if (clss.newInstance() instanceof HttpMessageOut) {
                        if (messageOutMap.containsKey(message.name())) {
                            Exception exception = new RuntimeException("Dupplicate HttpMessage: " + clss.getName());
                            Utils.error(TAG, exception);
                            throw exception;
                        } else {
                            messageOutMap.put(message.name(), clss);
                        }
                    }
                    Utils.log(TAG, "Reload HttpMessage: " + clss.getName());
                } else {
                    Exception exception = new RuntimeException("Cannot found HttpMessage: " + clss.getName());
                    Utils.error(TAG, exception);
                    throw exception;
                }
            } catch (Exception e) {
                Utils.error(TAG, e);
            }
        }
    }

    public static Class getClassMessageIn(String messageName) throws Exception {
        if (messageInMap.containsKey(messageName)) {
            Class clss = messageInMap.get(messageName);
            return clss;
        }
        return null;
    }

    public static HttpMessageIn getMessageIn(String messageName) throws Exception {
        if (messageInMap.containsKey(messageName)) {
            Class clss = messageInMap.get(messageName);
            return (HttpMessageIn) clss.newInstance();
        }
        return null;
    }
}
