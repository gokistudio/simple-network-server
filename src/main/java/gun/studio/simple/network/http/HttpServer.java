package gun.studio.simple.network.http;


import gun.studio.simple.network.common.Utils;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

public class HttpServer {
    private static final String TAG = HttpServer.class.getSimpleName();

    public static void start(int port) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                EventLoopGroup eventLoopGroup = new NioEventLoopGroup();
                try {
                    ServerBootstrap bootstrap = new ServerBootstrap()
                            .group(eventLoopGroup)
                            .handler(new LoggingHandler(LogLevel.INFO))
                            .childHandler(new ChannelInitializer<Channel>() {
                                @Override
                                protected void initChannel(Channel ch) throws Exception {
                                    ChannelPipeline pipeline = ch.pipeline();
                                    pipeline.addLast(new HttpServerCodec());
                                    pipeline.addLast("/", new HttpHandle());
                                }
                            })
                            .channel(NioServerSocketChannel.class);
                    Channel ch = bootstrap.bind(port).sync().channel();
                    Utils.log(TAG, "Http listener at port " + port);
                    ch.closeFuture().sync();
                } catch (Exception e) {
                    Utils.log(TAG, "Http listener error at port " + port);
                    Utils.error(TAG, e);
                } finally {
                    eventLoopGroup.shutdownGracefully();
                }
            }
        }).start();

    }
}
