package gun.studio.simple.network.http;

import com.fasterxml.jackson.databind.ObjectMapper;
import gun.studio.simple.network.common.Utils;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.*;
import io.netty.util.CharsetUtil;
import org.json.JSONObject;

import java.util.*;

public class HttpHandle extends SimpleChannelInboundHandler<Object> {
    private static final String TAG = HttpHandle.class.getSimpleName();

    protected void channelRead0(ChannelHandlerContext ctx, Object msg) throws Exception {
        try {
            if (msg instanceof HttpRequest) {
                HttpRequest request = (HttpRequest) msg;
                QueryStringDecoder stringDecoder = new QueryStringDecoder(request.uri());
                Map<String, List<String>> parameters = stringDecoder.parameters();
                String data = parameters.get("data").get(0);
                JSONObject jsonObject = new JSONObject(data);
                String messageName = jsonObject.getString("name");

                ObjectMapper objectMapper = new ObjectMapper();
                String messageBody = jsonObject.get("body").toString();
                Class clss = HttpMessageManager.getClassMessageIn(messageName);
                if (clss != null) {
                    HttpMessageIn messageIn = (HttpMessageIn) objectMapper.readValue(messageBody, clss);
                    if (messageIn.log()) {
                        Utils.log(TAG, "Receiver HttpMsg: " + messageIn.toString());
                    }
                    messageIn.setCtx(ctx);
                    messageIn.execute();
                } else {
                    ByteBuf content = Unpooled.copiedBuffer("Cannot find HttpMessageIn", CharsetUtil.UTF_8);
                    FullHttpResponse httpResponse = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK, content);
                    httpResponse.headers().set(HttpHeaderNames.CONTENT_TYPE, "application/json;charset=UTF-8");
                    httpResponse.headers().set(HttpHeaderNames.CONTENT_LENGTH, content.readableBytes());
                    httpResponse.headers().set("Access-Control-Allow-Origin", "*");
                    httpResponse.headers().set("Access-Control-Allow-Methods", "GET,POST");
                    httpResponse.headers().set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
                    ctx.write(httpResponse);
                    Utils.log(TAG, "Cannot find HttpMessageIn");
                }
            } else {
                Utils.log(TAG, "Not HttpRequest");
            }
        } catch (Exception e) {
            Utils.error(TAG, e);
            ByteBuf content = Unpooled.copiedBuffer(e.getMessage(), CharsetUtil.UTF_8);
            FullHttpResponse httpResponse = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK, content);
            httpResponse.headers().set(HttpHeaderNames.CONTENT_TYPE, "application/json;charset=UTF-8");
            httpResponse.headers().set(HttpHeaderNames.CONTENT_LENGTH, content.readableBytes());
            httpResponse.headers().set("Access-Control-Allow-Origin", "*");
            httpResponse.headers().set("Access-Control-Allow-Methods", "GET,POST");
            httpResponse.headers().set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            ctx.write(httpResponse);
        }
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        ctx.flush();
    }
}
